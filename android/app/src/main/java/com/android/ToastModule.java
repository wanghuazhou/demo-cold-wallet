package com.android;

import android.content.Intent;
import android.widget.Toast;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

public class ToastModule extends ReactContextBaseJavaModule {
    public ToastModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "Toasting";
    }

    @Nullable
    @Override
    public Map<String, Object> getConstants() {
        final Map<String, Object> map = new HashMap<>();

        map.put("SHORT", Toast.LENGTH_SHORT);
        map.put("LONG", Toast.LENGTH_LONG);

        return map;
    }


    @ReactMethod
    @SuppressWarnings("unused")
    public void show(String msg, int duration) {
        Toast.makeText(getReactApplicationContext(), msg, duration)
                .show();
    }
}